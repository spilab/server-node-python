'use strict';
const fs = require('fs');
var dateTime = require('node-datetime');

// BROKER --> !IMPORTANT --> COMMENT BROKER TO EXECUTE IN RPI
/*
var mosca = require('mosca');

var settings = {
    port:1883
}

var server = new mosca.Server(settings);

server.on('ready', function(){
    console.log("Broker ready");
});
*/
// END BROKER

// SERVER

var mqtt=require('mqtt');
const client = mqtt.connect("mqtt://localhost:1883");
var begin;
var end;

// MQTT connection
client.on("connect",function(){
    console.log("Server connected! Listening...");
	
	
	// Publish
	var json = {};
	
	json["hasName"] = "daniel";
	 /*
	if (mac == ""){
		json = {"hasName":name};
	} else {
		json = {"hasName":name, "macAddress":mac};
	}	
	*/
	
	console.log(" - Request: " + JSON.stringify(json));
	client.publish("request", JSON.stringify(json))
	
	//console.time("dbsave");
	begin=Date.now();
	
	
})

client.on('error', function () {
    logger.error({
        method: "connect(error)",
        arguments: arguments,
        cause: "likely MQTT issue - will automatically reconnect soon",
    }, "unexpected error");
});

//Listen MQTT
client.on('message', function (topic, message) {
	if (topic == "profile"){
		message= message.toString('utf8')
		var dt = dateTime.create();
		var formatted = dt.format('d-m-Y H:M:S');
		var json = JSON.parse(message);
		
		console.log("\n" + formatted + " - " + "MQTT msg - Topic: "+topic)
		console.log(json)
		
		end= Date.now();

		//var timeSpent=(end-begin)/1000+"secs";
		var timeSpent=(end-begin);
		
		console.log("Time="+timeSpent);
		timeSpent = timeSpent.toString() + "\n";
		
		const fs = require('fs');
		fs.appendFile("logComparison.txt", timeSpent, function(err) {
			if(err) {
				return console.log(err);
			}

			console.log("The file was saved!");
			client.end();
		}); 
		
		/*
		
		var name = json["hasName"];
		var mac = json["macAddress"];
		mac = mac.replace(/:/g,"");
		
		// Save profile to file
		var fileName = __dirname + "/profiles/" + name + "." + mac + ".json";
		let data = JSON.stringify(message);  	

		try{
			fs.writeFileSync(fileName, JSON.stringify(json)); 
			console.log("\n   -> REQ: Profile saved: " + "/profiles/" + name + "." + mac + ".json");
			
			// Modifing the ontology
			console.log(" - Adding data from " + fileName + " to the ontology");
			
			var spawn = require("child_process").spawn; 
			var process = spawn('python',["./spilab.py", "-c", fileName] ); 
		  
			process.stdout.on('data', function(data) { 
				console.log("   -> Data added successfully: " + data);
			});	
			
			process.on('close', function(code) {
				//console.timeEnd("dbsave");
				end= Date.now();

				//var timeSpent=(end-begin)/1000+"secs";
				var timeSpent=(end-begin);
				
				console.log("Time="+timeSpent);
				timeSpent = timeSpent.toString() + "\n";
				
				const fs = require('fs');
				fs.appendFile("logComparison.txt", timeSpent, function(err) {
					if(err) {
						return console.log(err);
					}

					console.log("The file was saved!");
				}); 
				
				
				
				//return callback(result);
			});
			
		}catch (ex){
			console.log("\n   -> REQ: Error ocurred. Profile do not saved: " + ex);
		}
		*/
		
		
		/*
		fs.writeFile(fileName, JSON.stringify(json), (err) => {  
			if (err) throw err;
			console.log("\n   -> REQ: Profile saved: " + "/profiles/" + name + "." + mac + ".json");
			
			// Modifing the ontology
			console.log(" - Adding data from " + fileName + " to the ontology");
			
			var spawn = require("child_process").spawn; 
			var process = spawn('python',["./spylab.py", "-c", fileName] ); 
			//var process = spawn('python',["spilab.py"] ); 
		  
			process.stdout.on('data', function(data) { 
				console.log("   -> Data added successfully");
			});		
		});
		*/
		
	} else {
		//message= message.toString('utf8')
		//console.log("MQTT msg - Topic: "+topic)
		//console.log(JSON.parse(message))
	}
   
});

//Tienes que suscribirte al topic donde van a a enviar los dispositivos moviles la informacion
client.subscribe("profile")
client.subscribe("request")


// END SERVER