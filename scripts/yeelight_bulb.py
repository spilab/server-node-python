from yeelight import discover_bulbs, Bulb
import sys

ip = ""
bulb = ""

def hex2rgb(value):
	value = value.lstrip('#')
	lv = len(value)
	return tuple(int(value[i:i+lv/3], 16) for i in range(0, lv, lv/3))

def sk_light_mode(sk_value):
	print ("	--- Setting state to", sk_value)
	if sk_value == "on":
		bulb.turn_on()
	elif sk_value == "off":
		bulb.turn_off()
	return

def sk_light_luminosity(sk_value):
	print ("	--- Setting luminosity to", sk_value)
	bulb.set_brightness(int(sk_value))
	return
	
def sk_light_rgb(sk_value):
	print ("	--- Setting color (rgb) to ", sk_value[0],sk_value[1],sk_value[2])
	bulb.set_rgb(sk_value[0],sk_value[1],sk_value[2])
	return

#print (len(sys.argv))
	
if len(sys.argv) > 2:
	#ip = sys.argv[1]	
	#sk = sys.argv[2]
	#sk_value = sys.argv[3]
	
	ip = ""
	id = ""
	sk = ""
	sk_value = ""
	
	bulb = ""
	
	i = 0
	for p in sys.argv:
		if p == "-ip":
			ip = sys.argv[i + 1]
		elif p == "-id":
			id = sys.argv[i + 1]
		elif p == "-sk":
			sk = sys.argv[i + 1]
			sk_value = sys.argv[i + 2]
		i = i + 1
		
	found = False
	# Check connection with bulb
	if ip != "":
		bulb = Bulb(ip)
		try:
			properties = (bulb.get_properties())
			if properties != "":
				print (" - Connection succesfully")
				found = True
		except Exception as e:
			print ("Error:",e)
			found = False
			
	if found == False:
		print (" - Not IP provided or unknown...discovering...")	
		# Get bulb ip
		bulbs = discover_bulbs()
		
		for b in bulbs:
			#print (b)
			#print (b["capabilities"]["id"])
			if b["capabilities"]["id"] == id:
				bulb = b
				ip = b["ip"]
				found = True
				
				#Setting the IP in the config file
				file = open("../config-files/yeelight.cfg", "r")
				print (" - Writing new IP")
				if file:
					data = json.load(file)
					data["IPAddress"] = ip
					with open("../config-files/yeelight.cfg", mode='a') as file:
						file.write(data)
					
	if found:
		print (" - Bulb found:", str(bulb))
		#print ("ip=", ip, "id=", id, "sk=", sk, "sk_value=", sk_value)
		
		bulb = Bulb(ip)
		
		if sk == "sk_light_mode":
			sk_light_mode(sk_value)
		elif sk == "sk_light_luminosity":
			sk_light_luminosity(sk_value)
		elif sk == "sk_light_rgb":
			#sk_value = hex2rgb(sk_value)
			
			if len(sk_value) == 6 or "#" in sk_value:
				sk_value = sk_value.lstrip('#')
				sk_value = tuple(int(sk_value[i:i+2], 16) for i in (0, 2, 4))
			else:
				sk_value = int(sk_value.split(",")[0]),int(sk_value.split(",")[1]),int(sk_value.split(",")[2])
			sk_light_rgb(sk_value)
		else:
			print ("\n	--- Skill DOES NOT recognized for Yeelight bulb\n")
	else :
		print (" - Bulb NOT found")
		
	
	
	
	
	#if sk == "sk_light_mode":
	#	sk_light_mode(sk_value)
	#elif sk == "sk_light_luminosity":
	#	sk_light_luminosity(sk_value)
	#elif sk == "sk_light_rgb":
	#	sk_value = hex2rgb(sk_value)
	#	sk_light_rgb(sk_value)
	#else:
	#	print ("\n	--- Skill DOES NOT recognized for Yeelight bulb\n")

else:
	print ("\n	--- Not arguments provided. 2 argument expected: [skill] [value]\n")
	#print ("Discovering...")
	#discover_bulbs(10)
	print ("\nEnd\n")

