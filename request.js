'use strict';

var name = "all";
var mac = "";
if (process.argv.length > 1) {
	name = process.argv[2];
	if (process.argv.length > 2){
		mac = process.argv[3];
	}
}
	
//Ejemplo de envio desde node
var mqtt=require('mqtt');

const client = mqtt.connect("mqtt://localhost");

client.on("connect",function(){
    //var json = {"hasName":"Daniel"}
	var json = {};
	
	if (mac != "")
		json["hasName"] = name;
	if (name != "")
		json["macAddress"] = mac;
	 /*
	if (mac == ""){
		json = {"hasName":name};
	} else {
		json = {"hasName":name, "macAddress":mac};
	}	
	*/
	
	console.log(" - Request: " + JSON.stringify(json));
	client.publish("request", JSON.stringify(json))
	client.end()
});

