from owlready2 import *
import yaml
import json
import subprocess
import datetime
import rdflib
import time
import os

# Dependencies
# sudo pip3 install Owlready2
# sudo pip3 install pyyaml
# sudo pip3 install yeelight
# sudo pip3 install rdflib

#ontoName = "./complete1.owl"
#ontoName = "./ontologies/skeleton.owl"
ontoName = "./ontologies/skeleton-modified.owl"
#ontoName = "http://www.lesfleursdunormal.fr/static/_downloads/pizza_onto.owl"
#ontoName = "http://homepages.laas.fr/nseydoux/ontologies/IoT-O.owl"
#ontoName = "http://iot.ee.surrey.ac.uk/fiware/ontologies/iot-lite/iot-lite.rdf"
#ontoName = "http://sensormeasurement.appspot.com/m3.owl"

#onto_path.append("/path/to/your/local/ontology/repository")
#onto = get_ontology("./complete1.owl")
#onto = get_ontology("http://www.lesfleursdunormal.fr/static/_downloads/pizza_onto.owl")
#onto = get_ontology("./skeleton.owl")
#onto = get_ontology("http://homepages.laas.fr/nseydoux/ontologies/IoT-O.owl") # Error 
#onto = get_ontology("http://iot.ee.surrey.ac.uk/fiware/ontologies/iot-lite/iot-lite.rdf")

onto = get_ontology(ontoName) 

logData = ""
start = "";
end = "";

fori = 0;
foriStart = 0;
foriEnd = 0;
foriAcum = 0;

class Entity(Thing):
	namespace = onto

class Service(Thing):
	namespace = onto

class Operation(Thing):
	namespace = onto
	
def loadOntology():
	
	onto.load()

	print ("Ontology loaded succesfully")
	return
	
def loadOntologyRDFlib():
	my_world = World()
	onto = my_world.get_ontology(ontoName).load()
	graph = my_world.as_rdflib_graph()
	return
	
def parseJSON(fileName):
	print ("\nReading:",str(fileName))
	print ("---------------")
	
	global logData
	
	file = open(fileName, "r")

	if file:
		data = json.load(file)
		#print (data)
		
		#print ("ID="+data["hasId"])
		#print ("Operations (goals)="+str(data["Operation"]))
		
		# Create the entity
		eName = data["hasName"]
		e = Entity(eName)
		
		# Get info
		e.hasId.append(data["hasId"])
		e.hasName.append(data["hasName"])
		e.IPAddress.append(data["IPAddress"])
		e.macAddress.append(data["macAddress"])
		
		logData = logData + data["hasId"] + "-" + data["hasName"] + "-" + data["macAddress"]
		
		# Get Operations (Goals)
		operations = data["Operation"]
		#print ("Operations:")
		for o in operations:
			optName = o["hasName"]
			powerValue = o["powerValue"]
			
			#print (name)
			#print (" - ",powerValue)
			
			# Create the service in the ontology
			try:
				destroy_entity(onto[optName])
			except:
				pass
			opt = Operation(optName)
			# Set operation properties
			onto[optName].powerValue.append(powerValue)
			# Associate the operation with the entity
			onto[eName].hasService.append(opt)
			
		
		# Get Services (Skills)
		services = data["Service"]
		#print ("Services:")
		for s in services:
			srvName = s["hasName"]
			hasAddress = s["hasAddress"]
			realStateValue = s["realStateValue"]
		
			#print (s["hasName"])
			#print (" - ",s["hasAddress"])
			#print (" - ",s["realStateValue"])
			
			# Create the service in the ontology
			#destroy_entity(onto[srvName])
			try:
				destroy_entity(onto[srvName])
			except:
				pass
			srv = Service(srvName)
						
			# Set service properties
			onto[srvName].realStateValue.append(realStateValue)
			onto[srvName].hasAddress.append(hasAddress)
			onto[srvName].IPAddress.append(data["IPAddress"])
			onto[srvName].hasId.append(data["hasId"])
			# Associate the service with the entity
			onto[eName].hasService.append(srv)
			
		onto.save(file = "./ontologies/skeleton-modified.owl", format = "rdfxml")
		

	return
	
def parseYaml():
	print ("\nCustomize.yaml")
	print ("---------------")
	stream = open("customize.yaml", "r")
	docs = yaml.load_all(stream)
	for doc in docs:
		for e,v in doc.items():
			# Get entity name
			print ("\nEntity :",e)
			
			# Check if present
			p = v.get("E_Present")
			
			if p:
			
				print (" - Present:",p)
				
				
				# Create the individual
				entity = Entity(e)
			
				# Get entity skills
				skills = v.get("skills")
				sJson = json.dumps(skills)
				ss = json.loads(sJson)
				print (" - Services: ")
				if sJson != "null":
					for key, value in ss.items():
						# Create the service
						#print ("    * ",key,"->", value)
						#print ("	* key=",key)
						
						sk_name = key
						sk_CurValue = value.get("sk_CurValue")
						sk_EndPoint = value.get("hasAddress")
						if sk_name != "" and sk_CurValue != "" and sk_EndPoint != "":		
							service = Service(sk_name)
							# Set service properties
							onto[sk_name].sk_CurValue.append(sk_CurValue)
							onto[sk_name].hasAddress.append(sk_EndPoint)
							# Associate the service with the entity
							onto[e].hasService.append(service)
						
				#print ("\t",skills)
				
				# Get entity goals
				goals = v.get("goals")		
				gJson = json.dumps(goals)
				gg = json.loads(gJson)
				print (" - Operations: ")
				if gJson != "null":
					for key, value in gg.items():
						# Create operation
						print ("    * ",key,"->", value)
						operation = Operation(key)
						
						# 
						destroy_entity(onto[key])
						
						# Associate the operation with the entity					
						onto[key].powerValue.append(value)
						onto[e].hasOperation.append(operation)
						
				#print ("\t",goals)
		print ("\n")

def matchOperationsServices():
	print ("\n")
	print ("Matching Operations and Services")
	print ("-------------")
	individuals = list(onto.individuals())
	for i in individuals:

		mod = onto.name+"."
		iStr = str(i).split(str(mod))[1]
		#print ("iStr=",iStr)		
		#g = Operation()
		if str(type(i)) == str(onto.name)+".Operation":					
		
			# Find associate service
			iOperation = Operation(str(iStr))
			#print (iOperation.name)
			#s = str(iStr)
			#s = s.replace("g_", "sk_")
			#operation = Operation(s)
			#print ("finding=", s)
			#s = "*"+str(operation.name)
			#print (s)
			
			skFind = "*"+str(i.name).replace("g_", "sk_")
			print ("Finding service for operation = "+i.name)
			
			res = onto.search(iri = skFind)
			if res:
				print ("    -> Found service: ", res, " -> ", i.name)
				service = str(res).replace("[","").replace("]","").split(".")[1]
				#iService = Service(service)
				#iOperation = Operation(i.name)
				
				#print ("iOperation="+i.name)
				#print ("iService="+service)
								
				linkOperationService(i.name, service)
				
				# Setting value
				val = str(onto[i.name].powerValue).replace("[","").replace("]","").replace("'","")
				#print ("VAL=",val)
				#print ("VAL0=",onto[i.name])
				#print ("VAL1=",onto[i.name].powerValue)
				#print (val)
				
				#print (onto[service].hasAddress)
				sk_value = val
				endp = str(onto[service].hasAddress).replace("[","").replace("]","").replace("'","")
				endpIp= str(onto[service].IPAddress).replace("[","").replace("]","").replace("'","")
				endpID = str(onto[service].hasId).replace("[","").replace("]","").replace("'","")
				
				print ("id=",onto[service].hasId)
				
				#print (endp, service, sk_value)
				#print (sk_value)
				#cmd = endp + " " + service + " " + sk_value
				
				# Get endpoint type (file or process)
				endSplit = endp.split(";")
				endType = endSplit[0]
				endContent = endSplit[1]
				
				if endType == "file":
					cmd = endp + " -ip " + endpIp + " -id " + endpID + " -sk " + service + " " + sk_value
					#cmd = endp + " " + service + " " + sk_value
					print ("EL CMD=",cmd)
					callEndpoint(str(cmd), "")
				elif endType == "subprocess":
					callEndpoint(str(endp), sk_value)
				else:
					print ("Unknown endpoint")
				
				#print ("1-",endp)
				#print ("2-",service)
				#print ("3-",sk_value)
				
				
				
				#print ("	-> Executting...'"+str(cmd)+"'")
				
				#os.system(cmd)
				
				print ("")
			else:
				print ("    -> Service NOT found")
			
		#print (i)
		#print (" -> ", type(i))
	saveOntology()
	return
	

def showOntology():
	print ("\n")
	print ("Ontology data")
	print ("-------------")
	list(onto.classes())
	print (" - Classes: ", list(onto.classes()))
	print (" - Properties: ", list(onto.properties()))
	print (" - Data properties: ", list(onto.data_properties()))
	print (" - Individuals: ", list(onto.individuals()))
	print ("\n")
	
	#s =  "illuminance"
	#search (s)
	#searchSPARQL(s)
	
	return
	
def showOntologyRDFlib():
	return
	
def search(e):
	print ("\n")
	print ("Native search")
	print ("-------------")
	print ("Searching for '"+e+"' into the ontology...")
	res = onto.search(produces = e, _case_sensitive = "false")
	
	print (" --> Result =",res)
	
	return
	
def searchSPARQL(e):
	print ("\n")
	print ("SPARQL search")
	print ("-------------")
	print ("Searching for '"+e+"' into the ontology with SPARQL query...")
	my_world = World()
	onto = my_world.get_ontology(ontoName).load()
	graph = my_world.as_rdflib_graph()
	q = """ 
	PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
	PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
	PREFIX m3: <http://sensormeasurement.appspot.com/m3#> 
	PREFIX dc: <http://purl.org/dc/elements/1.1/>
	PREFIX owl: <http://www.w3.org/2002/07/owl#>
	
	SELECT DISTINCT ?entity ?subc ?p ?v
	WHERE {
	  ?entity rdf:type owl:Class.
	  ?entity rdfs:subClassOf* ?subc .
	  ?subc rdf:type owl:Restriction.
	  ?subc owl:onProperty ?p .
	  ?subc owl:someValuesFrom ?v .
	  FILTER REGEX (str(?p), 'produces','i') .
	  FILTER REGEX (str(?v), '"""+e+"""','i') . } order by ?entity """
	
	r = list(graph.query(q))
	i = 0
	print (" --> Result =", r)
	print ("\n --> Records found =", len(r))
	#for row in r:
	#	print(row)
	#	i=i+1
		
	#rs = str(r).split(",")
	#for i in rs:
	#	print (i)
		
	print ("\n --> Query =", q)
	return

def showOperations():
	print ("\n")
	print ("All goals")
	print ("---------")
	for ind in onto.individuals():
		#print (ind.get_properties())
		for prop in ind.get_properties():
			#print ("ind =",ind)
			#print (str(ind).split(".")[1])
			#e = "onto."+(str(ind).split(".")[1]);
			#print (e)
			for value in prop[ind]:
				try:
					if value.powerValue:
						eName = ' '.join(str(ind).split(".")[1:])
						eName = eName.replace(" ",".")
						print(eName,".%s == %s" % (prop.python_name, value) + " == ",  value.powerValue)
				except:
					pass
	return
	
def showServices():	
	print ("\n")
	print ("All skills")
	print ("---------")
	for ind in onto.individuals():
		#print (ind.get_properties())
		for prop in ind.get_properties():
			#print ("ind =",ind)
			#print (str(ind).split(".")[1])
			#e = "onto."+(str(ind).split(".")[1]);
			#print (e)
			for value in prop[ind]:
				try:
					if value.sk_CurValue:
						eName = ' '.join(str(ind).split(".")[1:])
						eName = eName.replace(" ",".")
						print(eName,".%s == %s" % (prop.python_name, value) + " == ",  value.sk_CurValue)
				except:
					pass
	return
	
def createService():
	print ("New service")
	print ("--------------")
	#ind = Service("Dani_luminosity_rgb")
	#print (ind.name)	
		
	return
	
def saveOntology():
	onto.save(file = ontoName, format = "rdfxml")
	print("\nOntology saved")
	return
	
def linkOperationService(operation, service):
	print("     ** Linking operation and service **")
	print("             ", operation , " -> ", service)
	
	g = Operation(operation)
	sk = Service(service)
	
	gg = str(g.name).split(onto.name+".")[0]
	g = Operation(gg)
	
	sksk = str(sk.name).split(onto.name+".")[0]
	sk = Service(sksk)
	
	#print ("l_g=",g.name)
	#print ("l_sk=",sk.name)
		
	onto[gg].isImpactOf.append(sk)
	#onto[sksk].covers.append(g)
	
	#print ("1",onto[gg].isCoveredBy)
	#print ("2",onto[sksk].covers)
	
	#print ("------------LL-----------")
	#saveOntology()
	return
	
def callEndpoint(endpoint, value): # Value is optional
	print ("	-- Calling endpoint...")
	ep = endpoint.split(";")
	type = ep[0]
	endp = ep[1]
	print (type)
	print (endp)
	if type == "file":
		# Executting endpoint trought script
		try:
			endp = "./scripts/"+endp
			print ("	-> Executting script: ", endp)
			#os.system("python3 "+endp) # Raspi
			os.system("python "+endp) # Windows
			print ("	-> Endpoint executed succesfully -> ", endp,"\n")
		except:
			print ("	-> Endpoint execution error!\n")
		
	elif type == "subprocess":
		# Executtion endpoint as a process
		try:
			print ("	-> Executting process: ", endp, value)
			#subprocess.run([endp],value) # Windows
			fendp = endp + " " +str(value) # Raspi
			os.system(fendp) # Raspi
			print ("	-> Endpoint executed succesfully -> ", endp,"\n")
		except:
			print ("	-> Endpoint execution error!\n")
		
	else:
		print ("	-> Unknown endpoint\n")
	return
	
def executionLog():
	global logData
	global end
	global start
	global foriAcum
	global fori
	end = time.time() * 1000
	executionTime = end - start
	
	#foriAcum = foriAcum / fori
	
	log = str("Execution time average: " + executionTime + "\n")
	
	with open("logFori.txt", mode='a') as file:
		file.write(log)
	return
	
def executionLogFori():
	global logData
	global end
	global foriStart
	global foriEnd
	global fori
	global foriAcum
	foriEnd = time.time() * 1000
	executionTime = foriEnd - foriStart
	
	foriAcum = foriAcum + executionTime
	
	#foriAcum = float(foriAcum) + float(executionTime)
	#foriAcum = float(foriAcum)
	
	log = str(executionTime) + "\n"
	
	with open("logFori.txt", mode='a') as file:
		file.write(log)
	return
	
#loadOntology()
#showOntology()

#parseJSON("./config-files/daniel.cfg")
#parseJSON("./config-files/yeelight.cfg")

#loadOntologyRDFlib()
#showOntologyRDFlib()

#parseYaml()
#saveOntology()
#showOntology()

#showServices()
#showOperations()

#matchOperationsServices()
#saveOntology()

def main():
	if len(sys.argv) > 1:
		mode = sys.argv[1]	
		try:
		
			global start
			start = time.time() * 1000
		
			# Load Json entity config file
			if mode == "-config" or mode == "-c":		
				fileName = sys.argv[2]
				global fori
				global foriStart
				for fori in range(1, 25):
					foriStart = time.time() * 1000
					loadOntology()
					parseJSON(fileName)
					matchOperationsServices()
					executionLogFori()
			# Load Yaml entity config file
			elif mode == "-configy" or mode == "-cy":
				fileName = sys.argv[2]
				loadOntology()
				parseYaml(fileName)
			# Clear the ontology
			elif mode == "--clear":
				# clearOntology()
				pass
			elif mode == "-m" or mode == "--match":
				loadOntology()
				matchOperationsServices()
			elif mode == "--help" or mode == "-h":
				print ("Help:\n\n -c || --config [fileNameJson]: load the file (Json) into the ontology\n -cy || --configy [fileNameYaml]: load the file (Yaml) into the ontology\n -m || --match: match Operations and Services (Goals and Skills) in the current ontology\n - ")
			else:
				print ("\n--- Invalid params\n")
			executionLog()
		except Exception as e:
			print(e)
			print ("\n-- An error occured:",e)
			print ("\n -- Ending...")

	else:
		print ("\n--- Not arguments provided. Showing the ontology\n")
		loadOntology()
		showOntology()
		
	print ("\n-- End\n")
	
if __name__== "__main__":
  main()


