#Table of content

[TOC]

##Information
**HassioCS** is a prototype developed to adapt the Internet of Things (IoT) devices to people's preferences regardless of the manufacturer. Semantic web techniques have been used to achieve greater interoperability between devices from different manufacturers.

A server is in charge of managing the environment in which these devices are found. Using a script in Node.js, the server requests profiles from users through the MQTT protocol, which once received, are treated using a script in Python to integrate them into to the ontology of the environment through Semantic Web and to related them. Besides, the smart devices are configured according to users preferences.

##Get started
 1. Clone the repository:
 `$ git clone https://dafloresm@bitbucket.org/spilab/server-node-python.git`
 
 2. Enter the directory:
 `$ cd .\server-node-python`
 
 3. Get the dependencies:
 `$ npm install`

##How to use

### To set up a server
                
1. **Load the broker**: First, the broker may be executed to enable the MQTT communication with the devices:

 `$ node broker`

![](https://bitbucket.org/spilab/server-node-python/raw/6b1f9eaba7e52a11e26954d39ccd0cd62798276c/images/broker.png)

> Broker execution

Note: if you have another broker executting in your machine you can edit the index.js file to indicate its IP address.

2. **Execute the server**: Then, execute the server to keep waiting for profiles information:

 `$ node index`

![](https://bitbucket.org/spilab/server-node-python/raw/6b1f9eaba7e52a11e26954d39ccd0cd62798276c/images/server.png)

> Node.js server execution

### To request a profile to introduce it in the ontology
Once the server is running, it is able to request people profiles. These profiles are stored in the smartphones (check the [CSProfile](https://bitbucket.org/spilab/android/src/master/) Readme.md). To request a profile:

 `$ node request username`. E.g. `$ node request daniel`

![](https://bitbucket.org/spilab/server-node-python/raw/6b1f9eaba7e52a11e26954d39ccd0cd62798276c/images/request_profile.png)

> Profile request

Once the profile is received its information is loaded through the Python script to add the information to the ontology. Besides, the smart devies around the environment are adapted to the preferences contained in the profile, if posible.

![](https://bitbucket.org/spilab/server-node-python/raw/6b1f9eaba7e52a11e26954d39ccd0cd62798276c/images/profile_processing.png)

> Profile management

### To introduce a profile in the ontology manually
You can also load a profile manually if you do not have the smartphone application installed. These profiles are stored in 'profiles' (for people profiles) and 'config-files' (for smart devices). To load a profile:

 `$ python spilab.py -c 'profile.json'`. E.g.  `$ python spilab.py -c 'config-files/yeelight.cfg'`

![](https://bitbucket.org/spilab/server-node-python/raw/6b1f9eaba7e52a11e26954d39ccd0cd62798276c/images/add_profile.png)

> Adding a profile manually


##Video demo
A video demo is available [here](https://www.dropbox.com/s/0b879j1einsv6wq/Video%20prototype.mp4?dl=0).

##Contact
dfloresm@unex.es